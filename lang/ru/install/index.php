<?
$MESS["FORM_INSTALL_TITLE"] = "Установка модуля newsite.crmlead";
$MESS["FORM_UNINSTALL_TITLE"] = "Удаление модуля newsite.crmlead";

$MESS["MODULE_NAME"] = "Отслеживание изменения лидов и сделок";
$MESS["MODULE_DESCRIPTION"] = "Фиксирует любые изменения в лидах и сделках и изменяет поле \"Дата изменения\" лида или сделки, для дальнейшей фильтрации по ней, для выявления неактивных лидов и сделок.";
$MESS["PARTNER_NAME"] = "newsite.by";
$MESS["PARTNER_URI"] = "http://www.newsite.by";
?>
