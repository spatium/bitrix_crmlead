<? if (!check_bitrix_sessid()) return; ?>
<?

global $errors;

if ($errors === NULL) {
    echo CAdminMessage::ShowNote("Модуль newsite.crmlead установлен");
}
else {
    for ($i = 0; $i < count($errors); $i++) {
        $alErrors .= $errors[$i] . "<br>";
    }
    echo CAdminMessage::ShowMessage(Array("TYPE" => "ERROR", "MESSAGE" => GetMessage("MOD_INST_ERR"), "DETAILS" => $alErrors, "HTML" => true));
}
?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
    <p>
	<input type="hidden" name="lang" value="<?echo LANG?>">
	<input type="submit" name="" value="<?echo GetMessage("MOD_BACK")?>">	
    </p>
<form>