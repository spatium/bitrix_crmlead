<?
foreach ( glob(__DIR__."/lib/*.php") as $filename ) {require_once $filename; }

class NewsiteCrmLead
{

    public function __construct() { }

    function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        if($GLOBALS['APPLICATION']->GetGroupRight("main") < "R")
            return;

        $MODULE_ID = basename(dirname(__FILE__));

        $aMenu = array(
            "parent_menu" => "global_menu_content",
            "section" => $MODULE_ID,
            "sort" => 360,
            "text" => "newsite.crm.lead дата последнего изменения лида",
            "title" => 'newsite.crm.lead дата последнего изменения лида',
            "url" => "/bitrix/admin/".$MODULE_ID."_base_index.php",
            "icon" => "",
            "page_icon" => "",
            "items_id" => $MODULE_ID."_items",
            "more_url" => array(),
            "items" => array()
        );

        if (file_exists($path = dirname(__FILE__).'/admin'))
        {
            if ($dir = opendir($path))
            {
                $arFiles = array();

                while(false !== $item = readdir($dir))
                {
                    if (in_array($item,array('.','..','menu.php')))
                        continue;

                    if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.$MODULE_ID.'_'.$item))
                        file_put_contents($file,'<'.'? require($_SERVER["DOCUMENT_ROOT"]."/local/modules/'.$MODULE_ID.'/admin/'.$item.'");?'.'>');

                    $arFiles[] = $item;
                }

                sort($arFiles);

            }
        }

        //$aModuleMenu[] = $aMenu;
    }

}
?>
