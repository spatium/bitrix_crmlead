<? if (!check_bitrix_sessid()) return; ?>
<?

global $errors;

if ($errors === NULL) {
    echo CAdminMessage::ShowNote("Модуль успешно удален");
}
else {
    for ($i = 0; $i < count($errors); $i++) {
        $alErrors .= $errors[$i] . "<br>";
    }
    echo CAdminMessage::ShowMessage(Array("TYPE" => "ERROR", "MESSAGE" => GetMessage("MOD_UNINST_ERR"), "DETAILS" => $alErrors, "HTML" => true));
}
if ($ex = $APPLICATION->GetException()) {
    echo CAdminMessage::ShowMessage(Array("TYPE" => "ERROR", "MESSAGE" => GetMessage("MOD_UNINST_ERR"), "HTML" => true, "DETAILS" => $ex->GetString()));
}
?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
<p>
	<input type="hidden" name="lang" value="<?echo LANG?>"/>
	<input type="submit" name="" value="<?echo GetMessage("MOD_BACK")?>"/>	
</p>
<form>