<?
/**
 * Переопределения св-ва "Дел" (Звонок, Встреча, Письмо) для Лида и Сделки
 * Св-ва вызываются:
 *  OnActivityAdd           - CAllCrmActivity::Add();
 *  OnActivityUpdate        - CAllCrmActivity::Update();
 *  OnBeforeActivityDelete  - CAllCrmActivity::Delete();
 */
AddEventHandler('crm', 'OnActivityAdd',             array('NS_CrmCustom', 'OnActivityCustom'));
AddEventHandler('crm', 'OnActivityUpdate',          array('NS_CrmCustom', 'OnActivityCustom'));
AddEventHandler('crm', 'OnBeforeActivityDelete',    array('NS_CrmCustom', 'OnActivityCustom')); // OnBeforeActitvityDelete - события до удаления 'activity' из самой базы.

/**
 * Переопределения св-в "Задача" в делах для Лида и сделки.
 * Функциона задач находится в модулях crm и tasks
 */
AddEventHandler('crm', 'OnTasksAdd',            array('NS_CrmCustom', 'OnActivityCustom'));
AddEventHandler('crm', 'OnTasksUpdate',         array('NS_CrmCustom', 'OnActivityCustom'));

AddEventHandler('tasks', 'OnBeforeTaskAdd',     array('NS_CrmCustom', 'OnTasksEvent'));
AddEventHandler('tasks', 'OnBeforeTaskUpdate',  array('NS_CrmCustom', 'OnTasksEvent'));
AddEventHandler('tasks', 'OnBeforeTaskDelete',  array('NS_CrmCustom', 'OnTasksEvent'));
//AddEventHandler('tasks', 'OnBeforeTaskDelete',  array('NS_CrmCustom', 'OnActivityCustom'));

/**
 * Предложения для Лидов и Сделки
 * Св-ва вызываются:
 *  OnBeforeCrmQuoteAdd     - CAllCrmQuote::Add();
 *  OnBeforeCrmQuoteUpdate  - CAllCrmQuote::Update();
 *  OnBeforeCrmQuoteDelete  - CAllCrmQuote::Delete();
 */
AddEventHandler('crm', 'OnBeforeCrmQuoteAdd',    array('NS_CrmCustom', 'OnQuoteCustom'));
AddEventHandler('crm', 'OnBeforeCrmQuoteUpdate', array('NS_CrmCustom', 'OnQuoteCustom'));
AddEventHandler('crm', 'OnBeforeCrmQuoteDelete', array('NS_CrmCustom', 'OnQuoteCustom'));

/**
 * Класс переопределяет события, вызванные методами класса по работе с "Делами" (Задача, Звонок, Встреча)
 *      CrmActivity::Add()
 *      CrmActivity::Update() - 
 *      CrmActivity::Delete() -
 */
class NS_CrmCustom
{
    const LEAD = 1;
    const DEAL = 2;

    public static function OnTasksEvent($arEvent, $arFields)
    {
        if( is_int($arEvent) ) {
            $arEvent = CTasks::GetList(array(), array('ID' => $arEvent))->fetch();
        }

        self::debug($arEvent, '_event');

        if( is_array($arEvent) ) {
            $arTasks = CCrmActivity::GetList(array(), array('ASSOCIATED_ENTITY_ID' => $arEvent['PARENT_ID']))->fetch();
            
            self::onActivityCustom(false, $arTasks);
        }
    }

    /**
     * Функция обновления даты изменения Лида и Сделки
     * @param $_arFields    int | array  - id удаляемого элемента | массив для изменения\добавления данных
     */
    public static function OnQuoteCustom($_arFields)
    {
        // если пришло число, то операция удаления. Получаем данные элемента из таблицы
        if( is_int($_arFields) ) {
            $_arFields = CCrmQuote::GetByID($_arFields);
        }
        
        if( is_array($_arFields) ) 
        {
            if( $_arFields['LEAD_ID'] != NULL ) {
                self::update(intval($_arFields['LEAD_ID']), self::LEAD);
            } elseif( $_arFields['DEAL_ID'] != NULL ) {
                self::update(intval($_arFields['DEAL_ID']), self::DEAL);
            }
        }
    }

    /**
     * Обработка сущностей во вкладке "Дела"
     * OWNER_ID - id сущности
     * OWNER_TYPE_ID - id типа сущности: 1 - Лид; 2 - Сделка;
     *
     * @param $_ID          int - id сущности
     * @param $_arFields    array - параметры сущности 
     */
    public static function OnActivityCustom($_ID, $_arFields)
    {
        if( empty($_arFields['OWNER_ID']) ) {
            $_arFields = CCrmActivity::GetList(array(), array('ID' => $_ID))->fetch();
        }

        switch($_arFields['OWNER_TYPE_ID'])
        {
            // Лид
            case self::LEAD :
                self::update($_arFields['OWNER_ID'], self::LEAD);
                break;
            // Событие
            case self::DEAL :
                self::update($_arFields['OWNER_ID'], self::DEAL);
                break;
            default:
                return false;
        }
    }

    /**
     * Функция обновления лида, либо сделки
     * @param $_id      int - id элемента
     * @param $_objCrm  int - тип события: 1 - Лид; 2 - Сделка;
     * @param $_fields  array - поля для обновления
     * @return bool
     */
    private static function update($_id, $_objCrm, $_fields = array())
    {
        switch($_objCrm)
        {
            case self::LEAD :
                $crmObj = new CCrmLead;
                break;
            case self::DEAL :
                $crmObj = new CCrmDeal;
                break;
            default:
                return false;
        }

        return $crmObj->Update($_id, $_fields);
    }

    /**
     * Отладочная функция записи переменной-массива в файл
     * @param $_data        array - данные, которые записать в файл
     * @param $_filename    string - имя файла
     * @return bool
     */
    public static function debug($_data, $_filename, $_type = 'w')
    {
        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/' . $_filename . '.log', $_type);

        return fwrite($fp, print_r($_data, true));
    }
}
?>
