<?
ini_set('display_errors', true);
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang) - strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang . "/lang/", "/install/index.php"));

class newsite_crmlead extends CModule {

    var $MODULE_ID = "newsite.crmlead";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $WRITE_PATH;

    const MODULE_ID = "newsite.crmlead";

    public function __construct() {
        $arModuleVersion = array();

        $path = str_replace('\\', '/', __FILE__);
        $path = substr($path, 0, strlen($path) - strlen('/index.php'));
        include($path . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = GetMessage('MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('MODULE_DESCRIPTION');

        $this->PARTNER_NAME = GetMessage('PARTNER_NAME');
        $this->PARTNER_URI = GetMessage('PARTNER_URI');

        $this->WRITE_PATH = '/local/php_interface/';
    }

    public function DoInstall() {
        global $APPLICATION, $DB, $step;

        if (self::writeToInit() && self::makeUpdateDate()) {
            $this->InstallDB();

            $APPLICATION->IncludeAdminFile(GetMessage("FORM_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/install/step1.php");
        }
    }

    public function DoUninstall() {
        global $APPLICATION, $DB, $step;

        $this->UnInstallDB();

        $APPLICATION->IncludeAdminFile(GetMessage("FORM_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/install/unstep1.php");
    }
    
    private function makeUpdateDate() {
        global $DB;
        \CModule::IncludeModule('crm');
        require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/" . self::MODULE_ID . "/lib/events.php";
        
        $arActivites = array();
        
        // ЛИДЫ
        // получаем id-шник всех лидов
        $rsLeads = CCrmLead::GetList();
        $arLeads = array();
        while( $arLead = $rsLeads->fetch() ) {
            $arLeads[] = $arLead['ID'];
        }
        unset($rsLeads);
        // получаем дату последнего изменения активов для всех лидов
        foreach( $arLeads as $id ) {
            $rsActivites = CCrmActivity::GetList(array("LAST_UPDATED" => "DESC"), array("OWNER_ID" => $id, "OWNER_TYPE_ID" => NS_CrmCustom::LEAD));
            if( $arActivity = $rsActivites->fetch() ) {
                $arActivites[NS_CrmCustom::LEAD][$id] = $arActivity["LAST_UPDATED"];
            }
        }
        unset($arLeads);
        
        // СДЕЛКИ
        // получаем id-шник всех сделок
        $rsDeals = CCrmDeal::GetList();
        $arDeals = array();
        while( $arDeal = $rsDeals->fetch() ) {
            $arDeals[] = $arDeal['ID'];
        }
        unset($rsDeals);
        // получаем дату последнего изменения активов для всех сделок
        foreach( $arDeals as $id ) {
            $rsActivites = CCrmActivity::GetList(array("LAST_UPDATED" => "DESC"), array("OWNER_ID" => $id, "OWNER_TYPE_ID" => NS_CrmCustom::DEAL));
            if( $arActivity = $rsActivites->fetch() ) {
                $arActivites[NS_CrmCustom::DEAL][$id] = $arActivity["LAST_UPDATED"];
            }
        }
        unset($arDeals); 
        
        // sql-ники для обновления даты лидов
        foreach( $arActivites as $pid => $values ) {
            if( $pid == NS_CrmCustom::LEAD ) {
                foreach( $values as $id => $date ) {
                    $sqlLead = "UPDATE b_crm_lead SET `DATE_MODIFY` = " . $DB->CharToDateFunction($date) . " WHERE `ID` = " . $id . "; ";
                    $DB->Query($sqlLead);
                }
            } elseif( $pid == NS_CrmCustom::DEAL ) {
                foreach( $values as $id => $date ) {
                    $sqlLead = "UPDATE b_crm_deal SET `DATE_MODIFY` = " . $DB->CharToDateFunction($date) . " WHERE `ID` = " . $id . "; ";
                    $DB->Query($sqlLead);
                }
            }
        }
        
        return true;
    }

    private function writeToInit() {
        $WRITE_PATH = '/local/php_interface/';
        // проверка на наличие папки. Нет - создаем.
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $WRITE_PATH)) {
            // почему-то с подпапкой не получается создать, пришлось так
            mkdir($_SERVER['DOCUMENT_ROOT'] . '/local/', 0755);
            $bPID = mkdir($_SERVER['DOCUMENT_ROOT'] . '/local/' . 'php_interface/', '0755');

            if (!$bPID) {
                return false;
            }
        }

        $file = $_SERVER['DOCUMENT_ROOT'] . $WRITE_PATH . 'init.php';

        $tWrite = '<?\Bitrix\Main\Loader::includeModule("' . $this->MODULE_ID . '");?>';

        $fd = fopen($file, 'w+');
        $tText = fread($fd, filesize($file));

        $fd = fopen($file, 'w');
        $pid = fwrite($fd, $tWrite . $tText);

        if (!$pid) {
            return false;
        }
        fclose($fd);
        return true;
    }

    public function InstallDB($arParams = array()) {
        RegisterModule(self::MODULE_ID);
        return true;
    }

    public function UnInstallDB($arParams = array()) {
        UnRegisterModule(self::MODULE_ID);
        return true;
    }

    public function InstallEvents() {
        return true;
    }

    public function UnInstallEvents() {
        return true;
    }

}
